# Keycloak Documentation

Keycloak is an open source software product to allow single sign-on with Identity and Access Management
aimed at modern applications and services. As of March 2018 this WildFly community project is under the 
stewardship of Red Hat who use it as the upstream project for their RH-SSO product.

## Contents

- [General Info](#general-info)
- [Installation](#installation)
- [APIs](#APIs)
    - [Register](#register)
    - [GetAccessToken](#getAccessToken)
    - [Logout](#logout)
    - [GetUserInfo](#getUserInfo)
    - [UpdateUser](#updateUser)
    - [ResetPassword](#resetPassword)



### General Info

This document is for using Keycloak APIs.
I use Keycloak's APIs form this page: [Keycloak API](https://www.keycloak.org/docs-api/12.0/rest-api/#_overview)

### Installation
At first, you need to install keycloak on your machine.
To do that you can use two ways, one of them is installing keycloak on your machine directly and other ways
is using docker to install that.

For first one you can use this: [Install on machine](https://www.altenburger.io/posts/install_keycloak/)
For second one you can use this: [Install with docker](https://www.keycloak.org/getting-started/getting-started-docker)

After that you should know about some concepts in Keycloak such as: Realm, Client, User, ... .

Finally, you can set these environment variables to use Keycloak in our system properly. 
(to do this you can see .env.example file)

KEY_CLOAK_BASE_URL
KEY_CLOAK_REALM
KEY_CLOAK_PROTOCOL
KEY_CLOAK_CLIENT_ID
KEY_CLOAK_CLIENT_SECRET
KEY_CLOAK_GRANT_TYPE
KEY_CLOAK_ADMIN_USERNAME
KEY_CLOAK_ADMIN_PASSWORD

### APIs

Here we want to explain each api that we have developed.
All our APIs are in this file: [KeycloakService](App\Services\Keycloak\KeycloakService)

#### Register

With this API we can register a user in keycloak server by these fields:
firstName, lastName, email, password and enabled.

#### getAccessToken

With this API we can get an access_token for a user by these fileds: username, password.
note that username and email are the same in our system.


#### Logout

With this API we can log out a user in keycloak server by this filed: refreshToken.

#### GetUserInfo

With this API we can get information of a user in keycloak server by this filed: accessToken getting from getAccessToken API.

#### UpdateUser

With this API we can update a user in keycloak server by these fields:
userId, firstName, lastName, email, and enabled.

#### UpdateUser

With this API we can reset the password of a user in keycloak server by these fields:
userId, password.
