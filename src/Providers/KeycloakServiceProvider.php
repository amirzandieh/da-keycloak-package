<?php

namespace CustomPackages\KeycloakService\Providers;

use CustomPackages\KeycloakService\Services\Keycloak\KeycloakService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Http;

class KeycloakServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/../config/keycloak.php' => config_path('keycloak.php'),
            ],
            'config'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( 'KeycloakService',
            function ()
            {
                return new KeycloakService(app(Http::class));
            }
        );
    }
}