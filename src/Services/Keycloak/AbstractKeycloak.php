<?php

namespace CustomPackages\KeycloakService\Services\Keycloak;

use Illuminate\Support\Facades\Http;

abstract class AbstractKeycloak
{
    /**
     * @var string
     */
    private $configFileName = 'keycloak';

    /**
     * @var Http
     */
    protected Http $httpClient;

    /**
     * @var string
     */
    protected string $baseUrl;

    /**
     * @var string
     */
    protected string $realm;

    /**
     * @var string
     */
    protected string $protocol;
    /**
     * @var string
     */
    protected string $clientId;

    /**
     * @var string
     */
    protected string $clientSecret;

    /**
     * @var string
     */
    protected string $grantType;

    /**
     * @var string
     */
    protected string $adminUsername;

    /**
     * @var string
     */
    protected string $adminPassword;

    /**
     * @param Http $httpClient
     */
    public function __construct(Http $httpClient)
    {
        $this->setHttpClient($httpClient);
        $this->setBaseUrl(config( "{$this->configFileName}.base_url", null));
        $this->setRealm(config( "{$this->configFileName}.realm", null));
        $this->setProtocol(config( "{$this->configFileName}.protocol", null));
        $this->setClientId(config( "{$this->configFileName}.client_id", null));
        $this->setClientSecret(config( "{$this->configFileName}.client_secret", null));
        $this->setGrantType(config( "{$this->configFileName}.password", null));
        $this->setAdminUsername(config( "{$this->configFileName}.username", null));
        $this->setAdminPassword(config( "{$this->configFileName}.password", null));
    }

    /**
     * @return Http
     */
    public function getHttpClient(): Http
    {
        return $this->httpClient;
    }

    /**
     * @param Http $httpClient
     */
    public function setHttpClient(Http $httpClient): void
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return string
     */
    public function getRealm(): string
    {
        return $this->realm;
    }

    /**
     * @param string $realm
     */
    public function setRealm(string $realm): void
    {
        $this->realm = $realm;
    }

    /**
     * @return string
     */
    public function getProtocol(): string
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     */
    public function setProtocol(string $protocol): void
    {
        $this->protocol = $protocol;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret(string $clientSecret): void
    {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return string
     */
    public function getGrantType(): string
    {
        return $this->grantType;
    }

    /**
     * @param string $grantType
     */
    public function setGrantType(string $grantType): void
    {
        $this->grantType = $grantType;
    }

    /**
     * @return string
     */
    public function getAdminUsername(): string
    {
        return $this->adminUsername;
    }

    /**
     * @param string $adminUsername
     */
    public function setAdminUsername(string $adminUsername): void
    {
        $this->adminUsername = $adminUsername;
    }

    /**
     * @return string
     */
    public function getAdminPassword(): string
    {
        return $this->adminPassword;
    }

    /**
     * @param string $adminPassword
     */
    public function setAdminPassword(string $adminPassword): void
    {
        $this->adminPassword = $adminPassword;
    }
}
