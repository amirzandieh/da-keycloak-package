<?php

namespace CustomPackages\KeycloakService\Services\Keycloak;

use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class KeycloakService extends AbstractKeycloak
{
    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $password
     * @param bool $enabled
     *
     * @return array
     */
    public function register(string $firstName, string $lastName, string $email, string $password, bool $enabled = true): array
    {
        $url = $this->getBaseUrl() . "/auth/admin/realms/" . $this->getRealm() . "/users";
        $accessToken = $this->getAccessToken($this->getAdminUsername(), $this->getAdminPassword());

        try {
            $request = Http::withHeaders([
                'Authorization' => 'Bearer ' . $accessToken['data']['access_token'] ?? ""
            ])->post($url, [
                'firstName' => $firstName,
                'lastName' => $lastName,
                'email' => $email,
                'username' => $email,
                'enabled' => $enabled,
                'credentials' => [
                    [
                        'type' => 'password',
                        'value' => $password,
                        'temporary' => false
                    ]
                ]
            ]);
        } catch (\Exception $exception) {
            return [
                'status' => Response::HTTP_BAD_GATEWAY,
                'message' => $exception->getMessage()
            ];
        }

        if (in_array($request->status(), [Response::HTTP_OK, Response::HTTP_CREATED])) {
            return [
                'status' => $request->status(),
                'message' => __('messages.successful_operation')
            ];
        }

        return [
            'data' => [],
            'status' => $request->status(),
            'message' => $response['error_description'] ?? __('messages.error')
        ];
    }

    /**
     * @param string $userName
     * @param string $password
     *
     * @return array
     */
    public function getAccessToken(string $userName, string $password): array
    {
        $url = $this->getBaseUrl() . "/auth/realms/" . $this->getRealm() . "/protocol/" . $this->getProtocol() . "/token";

        try {
            $request = Http::asForm()->post($url, [
                'client_id' => $this->getClientId(),
                'client_secret' => $this->getClientSecret(),
                'username' => $userName,
                'password' => $password,
                'grant_type' => $this->getGrantType(),
            ]);
        } catch (\Exception $exception) {
            return [
                'data' => [],
                'status' => Response::HTTP_BAD_GATEWAY,
                'message' => $exception->getMessage()
            ];
        }

        $response = $request->json();
        if (in_array($request->status(), [Response::HTTP_OK, Response::HTTP_NO_CONTENT])) {
            return [
                'data' => [
                    'access_token' => $response['access_token'] ?? '',
                    'expires_in' => $response['expires_in'] ?? '',
                    'refresh_token' => $response['refresh_token'] ?? '',
                    'refresh_expires_in' => $response['refresh_expires_in'] ?? ''
                ],
                'status' => $request->status(),
                'message' => __('messages.successful_operation')
            ];
        }

        return [
            'data' => [],
            'status' => $request->status(),
            'message' => $response['error_description'] ?? __('messages.error')
        ];
    }

    /**
     * @param string $refreshToken
     *
     * @return array
     */
    public function logout(string $refreshToken): array
    {
        $url = $this->getBaseUrl() . "/auth/realms/" . $this->getRealm() . "/protocol/" . $this->getProtocol() . "/logout";

        try {
            $request = Http::asForm()->post($url, [
                'client_id' => $this->getClientId(),
                'client_secret' => $this->getClientSecret(),
                'refresh_token' => $refreshToken,
            ]);
        } catch (\Exception $exception) {
            return [
                'status' => Response::HTTP_BAD_GATEWAY,
                'message' => $exception->getMessage()
            ];
        }

        if (in_array($request->status(), [Response::HTTP_OK, Response::HTTP_NO_CONTENT])) {
            return [
                'status' => $request->status(),
                'message' => __('messages.successful_operation')
            ];
        }

        return [
            'status' => $request->status(),
            'message' => __('messages.error')
        ];
    }

    /**
     * @param string $accessToken
     *
     * @return array
     */
    public function getUserInfo(string $accessToken): array
    {
        $url = $this->getBaseUrl() . "/auth/realms/" . $this->getRealm() . "/protocol/" . $this->getProtocol() . "/userinfo";

        try {
            $request = Http::withHeaders([
                'Authorization' => 'Bearer ' . $accessToken
            ])->get($url);
        } catch (\Exception $exception) {
            return [
                'data' => [],
                'status' => Response::HTTP_BAD_GATEWAY,
                'message' => $exception->getMessage()
            ];
        }

        $response = $request->json();
        if (in_array($request->status(), [Response::HTTP_OK, Response::HTTP_NO_CONTENT])) {
            return [
                'data' => $response,
                'status' => $request->status(),
                'message' => __('messages.successful_operation')
            ];
        }

        return [
            'data' => [],
            'status' => $request->status(),
            'message' => $response['error_description'] ?? __('messages.error')
        ];
    }

    /**
     * @param string $userId
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     *
     * @return array
     */
    public function updateUser(string $userId, string $firstName = '', string $lastName = '', string $email = ''): array
    {
        $url = $this->getBaseUrl() . "/auth/admin/realms/" . $this->getRealm() . "/users/" . $userId;
        $accessToken = $this->getAccessToken($this->getAdminUsername(), $this->getAdminPassword());

        $attributes = [];

        if ($firstName) {
            $attributes['firstName'] = $firstName;
        }

        if ($lastName) {
            $attributes['lastName'] = $lastName;
        }

        if ($email) {
            $attributes['email'] = $email;
            $attributes['username'] = $email;
        }

        try {
            $request = Http::withHeaders([
                'Authorization' => 'Bearer ' . $accessToken['data']['access_token'] ?? ""
            ])->put($url, $attributes);
        } catch (\Exception $exception) {
            return [
                'status' => Response::HTTP_BAD_GATEWAY,
                'message' => $exception->getMessage()
            ];
        }

        if (in_array($request->status(), [Response::HTTP_OK, Response::HTTP_NO_CONTENT])) {
            return [
                'status' => $request->status(),
                'message' => __('messages.successful_operation')
            ];
        }

        return [
            'data' => [],
            'status' => $request->status(),
            'message' => $response['error_description'] ?? __('messages.error')
        ];
    }

    /**
     * @param string $userId
     * @param string $newPassword
     *
     * @return array
     */
    public function resetPassword(string $userId, string $newPassword): array
    {
        $url = $this->getBaseUrl() . "/auth/admin/realms/" . $this->getRealm() . "/users/" . $userId;
        $accessToken = $this->getAccessToken($this->getAdminUsername(), $this->getAdminPassword());

        try {
            $request = Http::withHeaders([
                'Authorization' => 'Bearer ' . $accessToken['data']['access_token'] ?? ""
            ])->put($url, [
                'credentials' => [
                    [
                        'type' => 'password',
                        'value' => $newPassword,
                        'temporary' => false
                    ]
                ]
            ]);
        } catch (\Exception $exception) {
            return [
                'status' => Response::HTTP_BAD_GATEWAY,
                'message' => $exception->getMessage()
            ];
        }

        if (in_array($request->status(), [Response::HTTP_OK, Response::HTTP_NO_CONTENT])) {
            return [
                'status' => $request->status(),
                'message' => __('messages.successful_operation')
            ];
        }

        return [
            'data' => [],
            'status' => $request->status(),
            'message' => $response['error_description'] ?? __('messages.error')
        ];
    }
}
