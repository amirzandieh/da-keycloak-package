<?php

return [
    /*
	 * The base url for authentication to Keycloak api.
	 */
    'base_url' => env( 'KEY_CLOAK_BASE_URL', 'http://localhost:8080'),

    /*
	 * The realm of Keycloak api.
	 */
    'realm' => env( 'KEY_CLOAK_REALM', 'master'),

    /*
	 * The protocol of Keycloak api.
	 */
    'protocol' => env( 'KEY_CLOAK_PROTOCOL', 'openid-connect'),

    /*
	 * The client id Keycloak api.
	 */
    'client_id' => env( 'KEY_CLOAK_CLIENT_ID', 'admin-cli'),

    /*
	 * The client secret Keycloak api.
	 */
    'client_secret' => env( 'KEY_CLOAK_CLIENT_SECRET', '7db34d80-162f-48c2-922b-a0859b26a5b1'),

    /*
	 * The grant type Keycloak api.
	 */
    'grant_type' => env( 'KEY_CLOAK_GRANT_TYPE', 'password'),

    /*
	 * The username Keycloak api.
	 */
    'username' => env( 'KEY_CLOAK_ADMIN_USERNAME', 'test'),

    /*
	 * The password Keycloak api.
	 */
    'password' => env( 'KEY_CLOAK_ADMIN_PASSWORD', 'test'),
];
